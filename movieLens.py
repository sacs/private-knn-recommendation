import numpy as np 
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.neighbors import NearestNeighbors

def recommend_item(user_index, similar_user_indices, matrix, items=5):
    
    # movie ratings by similar users
    similar_users = matrix[matrix.index.isin(similar_user_indices)]
    
    # avg of movie ratings by similar users
    similar_users = similar_users.mean(axis=0)
    
    # convert to dataframe
    similar_users_df = pd.DataFrame(similar_users, columns=['mean'])
    
    # row for the current user
    user_df = matrix[matrix.index == user_index]
    user_df_transposed = user_df.transpose()
    user_df_transposed.columns = ['rating']
    
    # remove movies that the user has watched
    user_df_transposed1 = user_df_transposed[user_df_transposed['rating']==0]
    movies_unseen = user_df_transposed1.index.tolist()
    
    # movies that the user has seen
    # user_df_transposed2 = user_df_transposed[user_df_transposed['rating']!=0]
    # movies_seen = user_df_transposed2.index.tolist()
    # movie_seen_df = df[(df['movieId'].isin(movies_seen)) & (df['userId']==current_user)]
    # movie_seen_df = movie_seen_df[['title','rating']]
    
    # filter movies to remove the ones that the user has watched
    similar_users_df_filtered = similar_users_df[similar_users_df.index.isin(movies_unseen)]
    similar_users_df_ordered = similar_users_df_filtered.sort_values(by=['mean'], ascending=False)
    
    # get the top n movies
    top_n_movies = similar_users_df_ordered.head(items)
    top_n_movie_indices = top_n_movies.index.tolist()
    
    # movie names
    movie_rec = df_movies[df_movies['movieId'].isin(top_n_movie_indices)]
    
    return movie_rec

df_movies=pd.read_csv('Data/movies.csv')
df_ratings=pd.read_csv('Data/ratings.csv')

df_movies.drop(columns='genres', inplace=True)
df_ratings.drop(columns='timestamp', inplace=True)
df = pd.merge(df_movies, df_ratings, on = "movieId")

df_ratings_pivot = df.pivot(index = 'userId', columns = 'movieId', values = 'rating').fillna(0)
df_ratings_matrix = csr_matrix(df_ratings_pivot.values)

#print(df_ratings_matrix)

model_knn = NearestNeighbors(metric = 'cosine', algorithm = 'brute')
model_knn.fit(df_ratings_matrix)

current_user = 223

# get the row for current user
distances, indices = model_knn.kneighbors(df_ratings_pivot.iloc[current_user-1,:].values.reshape(1, -1), n_neighbors = 6)

# add 1 to indice values (iloc uses indices, recommend_item uses ID's)
indices = [i+1 for i in indices]

print(recommend_item(current_user, indices[0][1:], df_ratings_pivot))