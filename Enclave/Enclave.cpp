#include <stdarg.h>
#include <stdio.h>      /* vsnprintf */
#include <string>
#include <vector>
#include <map>
#include <list>

#include "Enclave.h"
#include "Enclave_t.h"  /* print_string */

/* 
 * printf: 
 *   Invokes OCALL to display the enclave buffer to the terminal.
 */
void printf(const char *fmt, ...)
{
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
}

void printf_helloworld()
{
    printf("Hello World\n");
}

void recommendMovie_enclave(const char *indices)
{
    printf("Inside of recommendMovie_enclave!\n");
    //printf(indices);

    std::string i(indices);
    std::stringstream ss(i);
    std::string line;
    std::map<int, std::list<int> > mapOfIndices;

    while (std::getline(ss, line, '\n')) {
        std::string index;
        std::list<int> listOfIndices;

        std::size_t pos = line.find(","); //index of the first comma
        std::string indicesStr = line.substr(pos+1);  //rest of the line after the first element(userId)
		std::stringstream indicesStream(indicesStr);

        int userId = std::stoi(line.substr(0, pos));  //part of the line until the first comma
        

        while(std::getline(indicesStream, index, ',')){ 
            listOfIndices.push_back(std::stoi(index));
        }
        mapOfIndices[userId] = listOfIndices;
    }

}